import { useState } from "react";

const plans = [
  {
    name: "basic",
    price: 0,
  },
  {
    name: "elite",
    price: 3,
  },
  {
    name: "star",
    price: 8,
  },
  {
    name: "legend",
    price: 10,
  },
  {
    name: "pro",
    price: 15,
  },
];

const subPlans = [
  {
    name: "starter",
    price: 0,
  },
  {
    name: "medium",
    price: 3,
  },
  {
    name: "pro",
    price: 5,
  },
];

function App() {
  return (
    <div className="App">
      <ToggleMenu />
      <Subscription
        choosedPlan={plans}
        defaultPlan="legend"
        planName="plan-1"
      />
      <Subscription
        choosedPlan={subPlans}
        defaultPlan="medium"
        planName="plan-2"
      />
      <PasswordGenerator />
    </div>
  );
}

export default App;

function ToggleMenu() {
  const [isOpen, setIsOpen] = useState(false);

  function handleToggle() {
    setIsOpen(!isOpen);
  }

  return (
    <div>
      <h2>Toggle Menu</h2>

      <button onClick={handleToggle}>Toggle</button>
      {isOpen && (
        <div>
          <p>HOME</p>
          <p>ABOUT</p>
          <p>CONTACT</p>
        </div>
      )}
    </div>
  );
}

function Subscription({ choosedPlan, defaultPlan, planName }) {
  const [selectedPlan, setSelectedPlan] = useState(defaultPlan);

  function handleSelectPlan(e) {
    setSelectedPlan(e.target.value);
  }

  const colorRed = {
    color: "red",
  };

  return (
    <div>
      <h2>Subscription</h2>
      {choosedPlan.map((plan) => (
        <label key={plan.name}>
          <input
            type="radio"
            value={plan.name}
            name={planName}
            checked={selectedPlan === plan.name}
            onChange={handleSelectPlan}
          />
          {plan.name} - {plan.price === 0 ? "free" : `${plan.price}$`}
        </label>
      ))}

      <p>
        You have selected the <span style={colorRed}>{selectedPlan}</span> plan.
      </p>
    </div>
  );
}

function PasswordGenerator() {
  const [length, setLength] = useState(8);
  const [generatedPassword, setGeneratedPassword] = useState("");

  function shuffleString(inputString) {
    // Convert the string to an array of characters
    var charArray = inputString.split("");

    // Shuffle the array using Fisher-Yates algorithm
    for (var i = charArray.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = charArray[i];
      charArray[i] = charArray[j];
      charArray[j] = temp;
    }

    // Join the array back into a string
    var shuffledString = charArray.join("");

    return shuffledString;
  }

  function handleGeneratePassword() {
    const lowercase = "abcdefghijklmnopqrstuvwxyz";
    const uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const numbers = "0123456789";
    const special = "!@#$%^&*()_-+=<>?";

    const randomUppercase =
      uppercase[Math.floor(Math.random() * uppercase.length)];

    const randomNumber = numbers[Math.floor(Math.random() * numbers.length)];
    const randomSpecial = special[Math.floor(Math.random() * special.length)];

    let password = "";

    for (let i = 0; i < length - 3; i++) {
      password += lowercase[Math.floor(Math.random() * lowercase.length)];
    }

    var shuffledString = shuffleString(
      password + randomUppercase + randomNumber + randomSpecial
    );

    setGeneratedPassword(shuffledString);
  }

  const colorRed = {
    color: "red",
  };
  return (
    <div>
      <h2>Password Generator</h2>
      <input
        type="number"
        min="8"
        value={length}
        onChange={(e) => setLength(e.target.value)}
      />
      <button onClick={handleGeneratePassword}>Generate password</button>
      <p>
        your password: <span style={colorRed}>{generatedPassword}</span>
      </p>
    </div>
  );
}
